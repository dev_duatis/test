﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic;
using BusinessLogic.Contracts;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace DataAccess
{
    public class MongoDataContext : IDataContext
    {
        protected MongoClient client;
        protected IMongoDatabase db;

        public MongoDataContext(IDataContextSettings settings)
        {
            this.client = ( settings.ConnectionString != null )? new MongoClient(settings.ConnectionString) : new MongoClient();
            this.db = this.client.GetDatabase(settings.Database);
        }

        private IMongoCollection<T> collection<T>() where T: IModel
        {            
            return this.db.GetCollection<T>(typeof(T).Name);
        }

        public T Add<T>(T payload) where T : IModel
        {
            var col = this.collection<T>();

            col.InsertOne(payload);

            return payload;
        }

        public T Get<T>(string id) where T : IModel
        {
            var res = this.collection<T>().Find($"{{ _id: ObjectId('{id}') }}").FirstOrDefault();

            return res;
        }

        public IEnumerable<T> GetAll<T>() where T : IModel
        {
            var res = this.collection<T>().AsQueryable().ToList();

            return res;
        }

        public T Modify<T>(string id, T payload) where T : IModel
        {
            payload.Id = id;

            return this.collection<T>().FindOneAndReplace($"{{ _id: ObjectId('{id}') }}", payload);
        }

        public T Delete<T>(string id) where T : IModel
        {
            return this.collection<T>().FindOneAndDelete($"{{ _id: ObjectId('{id}') }}");
        }

        public T Get<T>(Func<T, bool> query) where T : IModel
        {
            return this.collection<T>().AsQueryable().Where(x => query(x)).FirstOrDefault();
        }

        public IEnumerable<T> GetMany<T>(Func<T, bool> query) where T : IModel
        {
            return this.collection<T>().AsQueryable().ToList().Where(query);
        }
    }
}
