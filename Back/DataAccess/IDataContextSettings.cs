﻿namespace DataAccess
{
    public interface IDataContextSettings
    {
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
