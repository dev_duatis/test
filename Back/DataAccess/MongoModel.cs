﻿using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace DataAccess
{
    public abstract class MongoModel
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId _id { get; set; }
        
        public string Id {
            get
            {
                return _id.ToString();
            }

            set
            {
                // this allow to work only with the Id field and comply with the BsonObject
                // generates a new _id if none provided
                this._id = value.Length > 0 ? new ObjectId(value) : new ObjectId();
            }
        }
        
    }
}
