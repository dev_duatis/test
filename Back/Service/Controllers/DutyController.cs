﻿using System;
using BusinessLogic.Contracts;
using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DutyController : ControllerBase
    {
        private readonly IDutyRepository repo;
        private readonly ILogger log;

        public DutyController(IDutyRepository _repo, ILogger<DutyController> _log)
        {
            this.repo = _repo;
            this.log = _log;
        }

        private ObjectResult solve(Func<object> action)
        {
            try
            {
                return Ok(action());
            }
            catch (Exception ex)
            {
                this.log.LogError(ex.Message);
                return Problem(ex.Message);
            }
        }

        [HttpGet]
        public ObjectResult Get()
        {
            return solve(() => this.repo.GetAll());
        }

        [HttpGet("{id}")]
        public ObjectResult Get(string id)
        {
            return solve(() => this.repo.Get(id));
        }

        [HttpPost]
        public ObjectResult Post([FromBody] Duty value)
        {
            return solve(() => this.repo.Add(value));
        }

        [Route("{id}")]
        public ObjectResult Put(string id, [FromBody] Duty value)
        {
            return solve(() => this.repo.Modify(id, value));
        }

        [HttpDelete("{id}")]
        public ObjectResult Delete(string id)
        {
            return solve(() => this.repo.Delete(id));
        }
    }
}
