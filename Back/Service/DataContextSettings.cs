﻿using System;
using DataAccess;

namespace Service
{
    public class DataContextSettings : IDataContextSettings
    {
        public DataContextSettings()
        {
            Database = "dimatica";
        }

        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
