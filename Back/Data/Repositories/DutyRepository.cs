﻿using System;
using System.Collections.Generic;
using BusinessLogic;
using BusinessLogic.Contracts;
using Data.Models;

namespace Data.Repositories
{
    public class DutyRepository : IDutyRepository
    {
        protected IDataContext dataContext;
        public DutyRepository(IDataContext _dataContext)
        {
            this.dataContext = _dataContext;
        }

        public virtual IDuty Add(IDuty payload)
        {
            return this.dataContext.Add((Duty)payload);
        }

        public IDuty Delete(string id)
        {
            return this.dataContext.Delete<Duty>(id);
        }

        public virtual IDuty Get(string id)
        {
            return this.dataContext.Get<Duty>(id);
        }

        public IDuty Get(Func<IDuty, bool> query)
        {
            return this.dataContext.Get<Duty>(query);
        }

        public virtual IEnumerable<IDuty> GetAll()
        {
            return this.dataContext.GetAll<Duty>();
        }

        public IEnumerable<IDuty> GetMany(Func<IDuty, bool> query)
        {
            return this.dataContext.GetMany<Duty>(query);
        }

        public virtual IDuty Modify(string id, IDuty payload)
        {
            return this.dataContext.Modify(id, (Duty)payload);
        }
    }
}
