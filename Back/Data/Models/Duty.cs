﻿using System;
using BusinessLogic.Contracts;
using DataAccess;

namespace Data.Models
{
    public class Duty : MongoModel, IDuty
    {
        public string Name { get; set; }
    }
}
