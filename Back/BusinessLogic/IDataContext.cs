﻿using System;
using System.Collections;
using System.Collections.Generic;
using BusinessLogic.Contracts;

namespace BusinessLogic
{
    public interface IDataContext 
    {
        public IEnumerable<T> GetAll<T>() where T : IModel;
        public T Get<T>(string id) where T : IModel;
        public T Get<T>(Func<T,bool> query) where T : IModel;
        public IEnumerable<T> GetMany<T>(Func<T,bool> query) where T : IModel;
        public T Add<T>(T payload) where T : IModel;
        public T Modify<T>(string id, T payload) where T : IModel;
        public T Delete<T>(string id) where T : IModel;
    }
}
