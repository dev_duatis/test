﻿using System;
namespace BusinessLogic.Contracts
{
    public interface IDutyRepository : IRepository<IDuty>
    {
    }
}
