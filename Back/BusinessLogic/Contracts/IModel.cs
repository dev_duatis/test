﻿using System;
namespace BusinessLogic.Contracts
{
    public interface IModel
    {
        public string Id { get; set; }
    }
}
