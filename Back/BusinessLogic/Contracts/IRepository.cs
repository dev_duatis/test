﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Contracts
{
    public interface IRepository<M>  where M : IModel
    {
        public M Get(string id);
        public M Get(Func<M,bool> query);
        public IEnumerable<M> GetAll();
        public IEnumerable<M> GetMany(Func<M, bool> query);
        public M Add(M payload);
        public M Modify(string id, M payload);
        public M Delete(string id);
    }
}
