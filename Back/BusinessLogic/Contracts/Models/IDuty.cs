﻿namespace BusinessLogic.Contracts
{
    public interface IDuty: IModel
    {
        public string Name { get; set; }
    }
}
