import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Duty } from 'src/app/models/duty';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-duty-list',
  templateUrl: './duty-list.component.html',
  styleUrls: ['./duty-list.component.css']
})
export class DutyListComponent implements OnInit {

  public duties: Duty[] = [];

  displayedColumns: string[] = ['id', 'name','actions'];

  constructor(private dataSvc: DataService, private router: Router) { }

  ngOnInit(): void {
    this.getList();
  }

  private getList()
  {
    this.dataSvc.GetAll()
    .then( res => this.duties = res )
    .catch( err => null );
  }

  delete( id: string): void{
    this.dataSvc.Delete(id)
    .then( res => this.getList())
    .catch( err => null );
  }

  edit( id: string): void{
    this.router.navigate([id])
  }

  new(): void
  {
    this.router.navigate(['new']);
  }

}
