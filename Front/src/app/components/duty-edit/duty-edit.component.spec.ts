import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyEditComponent } from './duty-edit.component';

describe('DutyEditComponent', () => {
  let component: DutyEditComponent;
  let fixture: ComponentFixture<DutyEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DutyEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
