import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Duty } from 'src/app/models/duty';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-duty-edit',
  templateUrl: './duty-edit.component.html',
  styleUrls: ['./duty-edit.component.css']
})
export class DutyEditComponent implements OnInit {

  private id: string;
  model: Duty;

  constructor(private dataSvc: DataService,private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => 
      {
        this.id = params['id'];

        if( this.id === 'new' )
        {
          this.model = new Duty();
          return;
        }

        this.dataSvc
        .Get(this.id)
        .then( res => this.model = res )
        .catch( err => null );
    });
  }

  save()
  {
    let p: Promise<Duty>;
    if(this.id === 'new')
    {
      p = this.dataSvc.Post(this.model);
    }
    else
    {
      p = this.dataSvc.Put(this.id,this.model);
    }

    p.then(res => this.router.navigate([""])).catch(err => null);
  }

}
