import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DutyEditComponent } from './components/duty-edit/duty-edit.component';
import { DutyListComponent } from './components/duty-list/duty-list.component';

const routes: Routes = [
  {path: "", component: DutyListComponent},
  {path: ":id", component: DutyEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
