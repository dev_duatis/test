import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Duty } from '../models/duty';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // this could come from a service
  private baseURl: string = 'http://localhost:5000/api/duty';

  constructor(private httpSvc: HttpClient) { }

  public GetAll(): Promise<Duty[]>
  {
    return this.httpSvc.get<Duty[]>(this.baseURl).toPromise();
  }

  public Get(id: string): Promise<Duty>
  {
    return this.httpSvc.get<Duty>(`${this.baseURl}/${id}`).toPromise();
  }

  public Delete(id: string): Promise<Duty>
  {
    return this.httpSvc.delete<Duty>(`${this.baseURl}/${id}`).toPromise();
  }

  public Post(payload: Duty): Promise<Duty>
  {
    return this.httpSvc.post<Duty>(this.baseURl,payload).toPromise();
  }

  public Put(id: string, payload: Duty): Promise<Duty>
  {
    return this.httpSvc.post<Duty>(`${this.baseURl}/${id}`,payload).toPromise();
  }
}
